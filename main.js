// Theme set and get
var M = localStorage.getItem("data-mode"), T = document.getElementById("theme");
function autom(C){localStorage.setItem("data-mode", C);document.documentElement.setAttribute("data-mode", C);}
if(M=="dark"||(M==null && window.matchMedia("(prefers-color-scheme: dark)").matches)){autom("dark")}else if(M=="light"||(M==null && window.matchMedia("(prefers-color-scheme: light)").matches)){autom("light")}else{autom("dark")}
// Theme switch
function theme(){M = localStorage.getItem("data-mode");if(M=="light"){autom("dark")}else{autom("light")}}
